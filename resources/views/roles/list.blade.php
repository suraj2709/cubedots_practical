@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Roles</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>S.No</th>
                                <th>Roles</th>
                                <th>Permissions</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @forelse($roles as $key => $role)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{ucwords($role->name)}}</td>
                                    <td>
                                        {{ucwords(str_replace("_"," ",implode(", ",$role->permissions()->pluck('name')->toArray())))}}
                                    </td>
                                    <td>
                                        <a href="{{route('change-permission',$role->id)}}" class="btn btn-primary">Change Permission</a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6">No role found.</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
