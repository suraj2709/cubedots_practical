@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Assign Permission for Role "{{ucwords($role->name)}}"</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="table-responsive">
                        <form action="{{route('update-permission',$role->id)}}" method="POSt">
                            @csrf
                            @method('PUT')
                            @foreach ($permissions as $key => $permission)
                            <div class="form-group">
                                <label for="permission{{$key}}">
                                    <input type="checkbox" name="{{$permission->id}}" id="permission{{$key}}" @if(in_array($permission->id,$user_roles)) checked="" @endif> {{ucwords(str_replace("_"," ",$permission->name))}}
                                </label>
                            </div>
                            @endforeach
                            <div class="form-group">
                                <input type="submit" class="btn btn-primry">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
