@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Edit Post "{{$post->title}}"</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <ul>
                        @foreach($errors->all() as $key => $error)
                        <li>{{$error}} </li>        
                        @endforeach
                        </ul>
                    </div>
                    @endif


                    <form action="{{route('posts.update',$post->slug)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="">Title :</label>
                            <input type="text" class="form-control" name="title" placeholder="Enter Post Title" required value="{{old("title",$post->title)}}">
                        </div>

                        <div class="form-group">
                            <label for="">Description :</label>
                            <textarea name="description" id="" class="form-control" placeholder="Enter Post Description" required>{{old("description",$post->description)}}</textarea>
                        </div>
                        <br>
                        <span>In case you wan to change featured image:</span>
                        <div class="form-group">
                            <label for="">Featured Image :</label>
                            <input type="file" name="image" class="form-control">
                            <label for="">Current Image</label> &nbsp; &nbsp; <img src="{{asset('storage/featured_image/thumbnail/'.$post->featured_image)}}" alt="" srcset="">
                        </div>

                        <div class="form-group">
                            <label for="">Tags</label>
                            <input type="text" name="tags" class="form-control" required placeholder="Enter Tags" value="{{old('tags',implode(",",$tags))}}">
                            <span style="color:red">For multiple tags please separate tags by ",". For e.g. HTML,PHP</span>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
