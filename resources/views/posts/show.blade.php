@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Post Detail of "{{$post->title}}"</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-3">
                        <img src="{{asset('storage/featured_image/'.$post->featured_image)}}" alt="thumbnail" style="width:100%">
                        </div>
                        <div class="col-md-9">
                        <h3>{{$post->title}}</h3>
                        <h6>Created By : {{$post->user->name ?? "--"}}</h6>
                        <h6>Created At: {{date('d-M-Y',strtotime($post->created_at))}}</h6>
                        <h6>Tags : {{implode(",",$post->tags()->pluck('tag_name')->toArray())}}</h6>
                        <p>
                            {{$post->description}}
                        </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection