@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">New Post</div>
                        <div class="col-md-3">
                            <form action="{{route('posts.index')}}" method="get">
                                <input type="text" class="form-control" name="search" placeholder="Search by tags">
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>S.No</th>
                                <th>Thumbnail</th>
                                <th>Title</th>
                                <th>Short Description</th>
                                <th>Tags</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @forelse($posts as $key => $post)
                                <tr>
                                    <td>{{$posts->firstItem() + $key}}</td>
                                    <td><img src="{{asset('storage/featured_image/thumbnail/'.$post->featured_image)}}" alt="thumbnail"></td>
                                    <td>{{ucfirst($post->title)}}</td>
                                    <td>{{Str::limit($post->description,30)}}..</td>
                                    <td>{{implode(",",$post->tags()->pluck('tag_name')->toArray())}}</td>
                                    <td>{{date('d-M-Y',strtotime($post->created_at))}}</td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton{{$key}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$key}}">
                                                <a class="dropdown-item" href="{{route('posts.show',$post->slug)}}">Detail</a>
                                                @if(Auth::user()->hasPermission('edit_post'))
                                                <a class="dropdown-item" href="{{route('posts.edit',$post->slug)}}">Edit</a>
                                                @endif
                                                @if(Auth::user()->hasPermission('delete_post'))
                                                <a class="dropdown-item" href="#" onclick="document.getElementById('deletePost{{$key}}').submit()">Delete</a>
                                                <form action="{{route('posts.destroy',$post->slug)}}" id="deletePost{{$key}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6">No new posts found.</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{$posts->appends(request()->query())->links()}}
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
