<div class="col-md-3">
    <div class="card">
        <div class="card-header">Menu</div>

        <div class="card-body">
            <ul>
                <li>
                    <a href="#">Posts</a>
                    <ul>
                        @if(Auth::user()->hasPermission('add_post'))
                        <li><a href="{{route('posts.create')}}">Create Post</a></li>
                        @endif
                        <li><a href="{{route('posts.index')}}">Post Lists</a></li>
                    </ul>
                </li>
                @if(Auth::user()->hasRole('superadmin'))
                <li>
                    <a href="{{route('roles')}}">Roles & Permissions</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</div>