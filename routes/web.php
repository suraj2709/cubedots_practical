<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::group(["middleware" => "auth"],function(){
    Route::resource('posts', 'PostController');
    Route::group(['middleware' => 'can:superadmin'], function () {
        Route::get('roles','RoleController@roles')->name('roles');
        Route::get('change-permissions/{role}','RoleController@changePermission')->name('change-permission');
        Route::put('update-permissions/{role}','RoleController@updatePermission')->name('update-permission');
    });
});

