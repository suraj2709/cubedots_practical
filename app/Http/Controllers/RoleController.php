<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
    Role,
    Permission
};
use Auth;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function roles()
    {
        $roles = Role::all();
        return view('roles.list',compact('roles'));
    }

    public function changePermission(Role $role)
    {
        $user_roles = $role->permissions->pluck('id')->toArray();
        $permissions = Permission::all();
        return view('roles.change-permission',compact('role','permissions','user_roles'));
    }

    public function updatePermission(Request $request,Role $role)
    {
        $permissions = $request->toArray();
        unset($permissions['_token']);
        unset($permissions['_method']);
        // dd();

        $role->permissions()->detach();
        $role->permissions()->attach(array_keys($permissions));

        return redirect()->route('roles')->with('status','Role permission updated successfully.');
    }
}
