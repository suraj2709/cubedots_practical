<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;

use App\{
    Post,
    PostTag
};
use Image,Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::orderBy('updated_at','desc');
        
        if($request->search){
            $posts->whereHas('tags',function(Builder $query){
                $query->where('tag_name','LIKE','%'.request()->search.'%');
            });
        }

        $posts = $posts->paginate(6);
        return view('posts.list',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->hasPermission('add_post'))
            abort(403,"Unauthorized Action.");

        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->hasPermission('add_post'))
            abort(403,"Unauthorized Action.");

        $this->validate($request,[
            "title" => "required|max:100",
            "description" => "required|max:1000",
            "image" =>  "required|image|max:1536",
            "tags"   =>  "required|max:200",
        ]);
        
        if($request->hasFile('image')) {
            $filenamewithextension = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('image')->storeAs('public/featured_image', $filenametostore);
            $request->file('image')->storeAs('public/featured_image/thumbnail', $filenametostore);
            
            $smallthumbnailpath = public_path('storage/featured_image/thumbnail/'.$filenametostore);
            $this->createThumbnail($smallthumbnailpath, 150, 93);
      
        }

        $post = new Post;
        $post->title = $request->title;
        $post->user_id = Auth::id();
        $post->slug = Str::slug($request->title." ".time(),"-");
        $post->description = $request->description;
        $post->featured_image = $filenametostore;
        $post->save();

        $tags = explode(",",$request->tags);
        $post_tags = [];
        foreach($tags as $tag){
            array_push($post_tags,["post_id" => $post->id,"tag_name" => $tag]);
        }

        PostTag::insert($post_tags);

        return redirect()->route('posts.index')->with('status','Post uploaded successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        if(!Auth::user()->hasPermission('edit_post'))
            abort(403,"Unauthorized Action.");

        $tags = $post->tags()->pluck('tag_name')->toArray();
        return view('posts.edit',compact('post','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Post $post)
    {
        if(!Auth::user()->hasPermission('edit_post'))
            abort(403,"Unauthorized Action.");
        
        $this->validate($request,[
            "title" => "required|max:100",
            "description" => "required|max:1000",
            "image" =>  "nullable|image|max:1536",
            "tags"   =>  "required|max:200",
        ]);
        
        if($request->hasFile('image')) {
            $filenamewithextension = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('image')->storeAs('public/featured_image', $filenametostore);
            $request->file('image')->storeAs('public/featured_image/thumbnail', $filenametostore);
            
            $smallthumbnailpath = public_path('storage/featured_image/thumbnail/'.$filenametostore);
            $this->createThumbnail($smallthumbnailpath, 150, 93);
      
        }

        $post->title = $request->title;
        $post->user_id = Auth::id();
        $post->slug = Str::slug($request->title." ".time(),"-");
        $post->description = $request->description;
        if($request->hasfile('image')){
            $post->featured_image = $filenametostore;
        }
        $post->save();

        $post->tags()->delete();
        
        $tags = explode(",",$request->tags);
        $post_tags = [];
        foreach($tags as $tag){
            array_push($post_tags,["post_id" => $post->id,"tag_name" => $tag]);
        }

        PostTag::insert($post_tags);

        return redirect()->route('posts.index')->with('status','Post updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if(!Auth::user()->hasPermission('delete_post'))
            abort(403,"Unauthorized Action.");
        
        $post->delete();

        return redirect()->route('posts.index')->with('status','Post deleted successfully.');
    }

    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
    }
}
