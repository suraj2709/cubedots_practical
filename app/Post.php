<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    public function tags()
    {
        return $this->hasMany(PostTag::class);
    }

    public static function boot()
    {
        parent::boot();    
        self::deleting(function($post) { // before delete() method call this
            $post->tags()->each(function($tag) {
               $tag->delete(); // <-- direct deletion
            });
            // do the rest of the cleanup...
       });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRouteKeyName()
    {
        return "slug";
    }
}
