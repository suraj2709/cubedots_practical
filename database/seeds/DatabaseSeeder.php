<?php

use Illuminate\Database\Seeder;
use App\{
    User,
    Role,
    Permission,
    PermissionRole
};

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(PermissionRoleSeeder::class);
    }


}

class UserSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        User::insert([
            [
                'name' => 'Super Admin',
                'email' => 'superadmin@gmail.com',
                'password' => Hash::make(12345678),
                'role_id' => 1,
            ],
            [
                'name' => 'Editor',
                'email' => 'editor@gmail.com',
                'password' => Hash::make(12345678),
                'role_id' => 2,
            ],
            [
                'name' => 'reader',
                'email' => 'reader@gmail.com',
                'password' => Hash::make(12345678),
                'role_id' => 3,
            ]
        ]
        );
    }

}

class RoleSeeder extends Seeder{
    public function run(){
        DB::table('roles')->delete();
        Role::insert([
            ["id" => 1,"name" => "superadmin"],
            ["id" => 2,"name" => "editor"],
            ["id" => 3,"name" => "reader"],
        ]);
    }
}

class PermissionSeeder extends Seeder{
    public function run()
    {
        DB::table('permissions')->delete();
        Permission::insert([
            ["id" => 1,"name" => "add_post"],
            ["id" => 2,"name" => "edit_post"],
            ["id" => 3,"name" => "delete_post"],
        ]);
    }
}

class PermissionRoleSeeder extends Seeder{
    public function run()
    {
        DB::table('permission_role')->delete();
        PermissionRole::insert([
            ["role_id" => 1,"permission_id" => "1"],
            ["role_id" => 1,"permission_id" => "2"],
            ["role_id" => 1,"permission_id" => "3"],
            ["role_id" => 2,"permission_id" => "1"],
            ["role_id" => 2,"permission_id" => "2"],
        ]);
    }
}
